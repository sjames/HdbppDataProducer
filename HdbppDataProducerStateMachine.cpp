/*----- PROTECTED REGION ID(HdbppDataProducerStateMachine.cpp) ENABLED START -----*/
//=============================================================================
//
// file :        HdbppDataProducerStateMachine.cpp
//
// description : State machine file for the HdbppDataProducer class
//
// project :     HdbppDataProducer
//
// This file is part of Tango device class.
// 
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
// 
//
// Copyright (C): 2015
//                European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                France
//
//=============================================================================
//                This file is generated by POGO
//        (Program Obviously used to Generate tango Object)
//=============================================================================

#include <HdbppDataProducer.h>

/*----- PROTECTED REGION END -----*/	//	HdbppDataProducer::HdbppDataProducerStateMachine.cpp

//================================================================
//  States  |  Description
//================================================================
//  ON      |  
//  OFF     |  


namespace HdbppDataProducer_ns
{
//=================================================
//		Attributes Allowed Methods
//=================================================

//--------------------------------------------------------
/**
 *	Method      : HdbppDataProducer::is_setState_allowed()
 *	Description : Execution allowed for setState attribute
 */
//--------------------------------------------------------
bool HdbppDataProducer::is_setState_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Not any excluded states for setState attribute in Write access.
	/*----- PROTECTED REGION ID(HdbppDataProducer::setStateStateAllowed_WRITE) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	HdbppDataProducer::setStateStateAllowed_WRITE

	//	Not any excluded states for setState attribute in read access.
	/*----- PROTECTED REGION ID(HdbppDataProducer::setStateStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	HdbppDataProducer::setStateStateAllowed_READ
	return true;
}


//=================================================
//		Commands Allowed Methods
//=================================================


/*----- PROTECTED REGION ID(HdbppDataProducer::HdbppDataProducerStateAllowed.AdditionalMethods) ENABLED START -----*/

//	Additional Methods

/*----- PROTECTED REGION END -----*/	//	HdbppDataProducer::HdbppDataProducerStateAllowed.AdditionalMethods

}	//	End of namespace
