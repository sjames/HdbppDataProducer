// Copyright (C) : 2014-2017
// European Synchrotron Radiation Facility
// BP 220, Grenoble 38043, FRANCE
//
// This file is part of HdbppDataProducer
//
// HdbppDataProducer is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// HdbppDataProducer is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with HdbppDataProducer.  If not, see <http://www.gnu.org/licenses/>.

#ifndef DATA_GEN_ATTRIBUTE_H
#define DATA_GEN_ATTRIBUTE_H

namespace HdbppDataProducer_ns
{
class DataGenAttribute
{
public:

    virtual void generate_data() = 0;

};

} // namespace HdbppDataProducer_ns

#endif //	DATA_GEN_ATTRIBUTE_H