// Copyright (C) : 2014-2017
// European Synchrotron Radiation Facility
// BP 220, Grenoble 38043, FRANCE
//
// This file is part of HdbppDataProducer
//
// HdbppDataProducer is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// HdbppDataProducer is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with HdbppDataProducer.  If not, see <http://www.gnu.org/licenses/>.

#ifndef SPECTRUM_ATTTIB_H
#define SPECTRUM_ATTTIB_H

#include "DataGenAttribute.h"

#include <memory>
#include <tango.h>

namespace HdbppDataProducer_ns
{
template<class T>
class SpectrumAttribute : public Tango::SpectrumAttr, public DataGenAttribute
{
public:
    SpectrumAttribute(const char *name, long data_type, Tango::AttrWriteType w_type, long max_dim_x)
        : Tango::SpectrumAttr(name, data_type, w_type, max_dim_x), _w_type(w_type)
    {
        _dim_x = max_dim_x ? max_dim_x : 1;
        _spectrum = std::make_unique<T[]>(_dim_x);
    }

    virtual ~SpectrumAttribute() {}

    virtual void read(Tango::DeviceImpl *, Tango::Attribute &attr) { attr.set_value(_spectrum.get(), _dim_x); }
    virtual void write(Tango::DeviceImpl *, Tango::WAttribute &) {}
    virtual bool is_allowed(Tango::DeviceImpl *, Tango::AttReqType) { return true; }

    // DataGenAttribute API
    virtual void generate_data() {}

private:

    std::unique_ptr<T[]> _spectrum;
    long _dim_x;
    Tango::AttrWriteType _w_type;
};

} // namespace HdbppDataProducer_ns

#endif //	SPECTRUM_ATTTIB_H
