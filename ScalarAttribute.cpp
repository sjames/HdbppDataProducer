// Copyright (C) : 2014-2017
// European Synchrotron Radiation Facility
// BP 220, Grenoble 38043, FRANCE
//
// This file is part of HdbppDataProducer
//
// HdbppDataProducer is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// HdbppDataProducer is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with HdbppDataProducer.  If not, see <http://www.gnu.org/licenses/>.

#include "ScalarAttribute.h"

#include <experimental/random>
#include <random>

using namespace Tango;

namespace HdbppDataProducer_ns
{

//=============================================================================
//=============================================================================
template<>
ScalarAttribute<Tango::DevString>::ScalarAttribute(const char *name, long data_type, AttrWriteType w_type)
    : Attr(name, data_type, w_type), _w_type(w_type)
{
    _scalar = std::make_unique<Tango::DevString>(nullptr);
}

//=============================================================================
//=============================================================================
template<>
ScalarAttribute<Tango::DevState>::ScalarAttribute(const char *name, long data_type, AttrWriteType w_type)
    : Attr(name, data_type, w_type), _w_type(w_type)
{
    _scalar = std::make_unique<Tango::DevState>(Tango::OFF);
}

//=============================================================================
//=============================================================================
template<>
void ScalarAttribute<Tango::DevBoolean>::generate_data()
{
    random_device rd;
    mt19937 gen(rd());
    bernoulli_distribution d(0.5);
    *_scalar = d(gen);
}

//=============================================================================
//=============================================================================
template<>
void ScalarAttribute<Tango::DevUChar>::generate_data()
{
    random_device rd;
    mt19937 gen(rd());
    uniform_int_distribution<int> d(0, 255);
    *_scalar = d(gen);
}

//=============================================================================
//=============================================================================
template<>
void ScalarAttribute<Tango::DevUShort>::generate_data()
{
    short x = experimental::randint(-1, 1);
    short result = (short)*_scalar + x;

    if (result < 0)
        *_scalar = 0;
    else
        *_scalar = result;
}

//=============================================================================
//=============================================================================
template<>
void ScalarAttribute<Tango::DevShort>::generate_data()
{
    int x = experimental::randint(-1, 1);
    long result = *_scalar + x;

    if (result < 0)
        *_scalar = 0;
    else
        *_scalar = result;
}

//=============================================================================
//=============================================================================
template<>
void ScalarAttribute<Tango::DevULong>::generate_data()
{
    int x = experimental::randint(-1, 1);
    long result = *_scalar + x;

    if (result < 0)
        *_scalar = 0;
    else
        *_scalar = result;
}

//=============================================================================
//=============================================================================
template<>
void ScalarAttribute<Tango::DevLong>::generate_data()
{
    int x = experimental::randint(-1, 1);
    long result = *_scalar + x;

    if (result < 0)
        *_scalar = 0;
    else
        *_scalar = result;
}

//=============================================================================
//=============================================================================
template<>
void ScalarAttribute<Tango::DevULong64>::generate_data()
{
    int x = experimental::randint(-1, 1);
    long result = *_scalar + x;

    if (result < 0)
        *_scalar = 0;
    else
        *_scalar = result;
}

//=============================================================================
//=============================================================================
template<>
void ScalarAttribute<Tango::DevLong64>::generate_data()
{
    int x = experimental::randint(-1, 1);
    long result = *_scalar + x;

    if (result < 0)
        *_scalar = 0;
    else
        *_scalar = result;
}

//=============================================================================
//=============================================================================
template<>
void ScalarAttribute<Tango::DevFloat>::generate_data()
{
    static int deg = 0;
    static double PI = 3.1415926;

    auto r = deg * PI / 180.0;
    *_scalar = cos(r);

    if (*_scalar > 1)
        *_scalar = 1;

    if (*_scalar < -1)
        *_scalar = -1;

    if (++deg == 360)
        deg = 0;
}

//=============================================================================
//=============================================================================
template<>
void ScalarAttribute<Tango::DevDouble>::generate_data()
{
    static int deg = 0;
    static double PI = 3.1415926;

    auto r = deg * PI / 180.0;
    *_scalar = sin(r);

    if (*_scalar > 1)
        *_scalar = 1;

    if (*_scalar < -1)
        *_scalar = -1;

    if (++deg == 360)
        deg = 0;
}

//=============================================================================
//=============================================================================
template<>
void ScalarAttribute<Tango::DevString>::generate_data()
{
    static vector<string> strings = {"this", "is", "a", "classic,", "a", "dish", "that", "will", "never", "go", "out",
        "of", "fashion", "If", "you're", "looking", "for", "a", "treat", "or", "to", "impress", "someone,", "this",
        "is", "at", "the", "top", "of", "my", "list.", "Whenever", "I", "make", "this", "recipe,", "I", "look",
        "forward", "to", "the", "moment", "when", "the", "steaks", "are", "returned", "to", "the", "pan", "to", "be",
        "covered", "in", "sauce.", "At", "that", "point,", "I", "just", "know", "how", "good", "it's", "going", "to",
        "taste"};

    if(*_scalar != nullptr)            
        CORBA::string_free(*_scalar);

    *_scalar = CORBA::string_dup(strings[experimental::randint(0, ((int)strings.size()) - 1)].c_str());
}

//=============================================================================
//=============================================================================
template<>
void ScalarAttribute<Tango::DevState>::generate_data()
{
    random_device rd;
    mt19937 gen(rd());
    bernoulli_distribution d(0.25);
    *_scalar = d(gen) ? Tango::ON : Tango::OFF;
}

} // namespace HdbppDataProducer_ns