# HdbppDataProducer

Small device server to produce data on all supported attribute types in the hdb++ archive system. 

## Cloning

```
git clone git@gitlab.esrf.fr:sjames/HdbppDataProducer.git
```

## Building and Installation

### Dependencies

The project has the following dependencies.

#### Project Dependencies

* Tango Controls 9 or higher.
* omniORB release 4 or higher.
* libzmq - libzmq3-dev or libzmq5-dev.

#### Toolchain Dependencies

* C++11 compliant compiler.
* CMake 3.0 or greater is required to perform the build.

### Build

Instructions on building the project.

```bash
cd project_name
mkdir build
cd build
cmake ../
make
```