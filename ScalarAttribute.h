// Copyright (C) : 2014-2017
// European Synchrotron Radiation Facility
// BP 220, Grenoble 38043, FRANCE
//
// This file is part of HdbppDataProducer
//
// HdbppDataProducer is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// HdbppDataProducer is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with HdbppDataProducer.  If not, see <http://www.gnu.org/licenses/>.

#ifndef SCARLAR_ATTTIB_H
#define SCARLAR_ATTTIB_H

#include <tango.h>
#include "DataGenAttribute.h"

namespace HdbppDataProducer_ns
{
template<class T>
class ScalarAttribute : public Tango::Attr, public DataGenAttribute
{
public:
    ScalarAttribute(const char *name, long data_type, Tango::AttrWriteType w_type)
        : Tango::Attr(name, data_type, w_type), _w_type(w_type)
    {
        _scalar = std::make_unique<T>();
    }

    virtual ~ScalarAttribute() {}

    virtual void read(Tango::DeviceImpl *, Tango::Attribute &attr) { attr.set_value(_scalar.get()); }
    virtual void write(Tango::DeviceImpl *, Tango::WAttribute &) {}
    virtual bool is_allowed(Tango::DeviceImpl *, Tango::AttReqType) { return true; }

    // DataGenAttribute API
    virtual void generate_data() {}

private:

    std::unique_ptr<T> _scalar;
    Tango::AttrWriteType _w_type;
};

} // namespace HdbppDataProducer_ns

#endif //	SCARLAR_ATTTIB_H