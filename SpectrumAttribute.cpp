// Copyright (C) : 2014-2017
// European Synchrotron Radiation Facility
// BP 220, Grenoble 38043, FRANCE
//
// This file is part of HdbppDataProducer
//
// HdbppDataProducer is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// HdbppDataProducer is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with HdbppDataProducer.  If not, see <http://www.gnu.org/licenses/>.

#include "SpectrumAttribute.h"

#include <cmath>
#include <complex>
#include <experimental/random>
#include <random>

using namespace Tango;

namespace HdbppDataProducer_ns
{

//=============================================================================
//=============================================================================
template<>
SpectrumAttribute<Tango::DevString>::SpectrumAttribute(
    const char *name, long data_type, AttrWriteType w_type, long max_dim_x)
    : Tango::SpectrumAttr(name, data_type, w_type, max_dim_x), _w_type(w_type)
{
    _dim_x = max_dim_x ? max_dim_x : 1;
    _spectrum = std::make_unique<Tango::DevString[]>(_dim_x);

    for (auto i = 0; i < _dim_x; i++)
        _spectrum[i] = nullptr;
}

//=============================================================================
//=============================================================================
template<>
SpectrumAttribute<Tango::DevString>::~SpectrumAttribute()
{
    for (auto i = 0; i < _dim_x; i++)
        if (_spectrum[i] != nullptr)
            CORBA::string_free(_spectrum[i]);
}

//=============================================================================
//=============================================================================
template<>
void SpectrumAttribute<Tango::DevBoolean>::generate_data()
{
    random_device rd;
    mt19937 gen(rd());
    bernoulli_distribution d(0.5);

    for (auto x = 0; x < _dim_x; x++)
        _spectrum[x] = d(gen);
}

//=============================================================================
//=============================================================================
template<>
void SpectrumAttribute<Tango::DevUChar>::generate_data()
{
    random_device rd;
    mt19937 gen(rd());
    uniform_int_distribution<int> d(0, 255);

    for (auto x = 0; x < _dim_x; x++)
        _spectrum[x] = d(gen);
}

//=============================================================================
//=============================================================================
template<>
void SpectrumAttribute<Tango::DevUShort>::generate_data()
{
    random_device rd;
    mt19937 gen(rd());

    normal_distribution<> d{5, 1};

    auto x = 0;

    while (x < _dim_x)
    {
        double number = round(d(gen));

        if ((number >= 3.0) && (number < 10.0))
            _spectrum[x++] = number;

    }
}

//=============================================================================
//=============================================================================
template<>
void SpectrumAttribute<Tango::DevShort>::generate_data()
{
    random_device rd;
    mt19937 gen(rd());

    normal_distribution<> d{0, 5};

    auto x = 0;

    while (x < _dim_x)
    {
        double number = round(d(gen));

        if ((number > -10.0) && (number < 10.0))
            _spectrum[x++] = number;
    }
}

//=============================================================================
//=============================================================================
template<>
void SpectrumAttribute<Tango::DevULong>::generate_data()
{
    random_device rd;
    mt19937 gen(rd());
    geometric_distribution<> d(0.5);

    for (auto x = 0; x < _dim_x; x++)
        _spectrum[x] = d(gen);
}

//=============================================================================
//=============================================================================
template<>
void SpectrumAttribute<Tango::DevLong>::generate_data()
{
    random_device rd;
    mt19937 gen(rd());
    geometric_distribution<> d(0.5);

    for (auto x = 0; x < _dim_x; x++)
        _spectrum[x] = d(gen);
}

//=============================================================================
//=============================================================================
template<>
void SpectrumAttribute<Tango::DevULong64>::generate_data()
{
    random_device rd;
    mt19937 gen(rd());
    geometric_distribution<> d(0.75);

    for (auto x = 0; x < _dim_x; x++)
        _spectrum[x] = d(gen);
}

//=============================================================================
//=============================================================================
template<>
void SpectrumAttribute<Tango::DevLong64>::generate_data()
{
    random_device rd;
    mt19937 gen(rd());
    geometric_distribution<> d(0.75);

    for (auto x = 0; x < _dim_x; x++)
        _spectrum[x] = d(gen);
}

//=============================================================================
//=============================================================================
template<>
void SpectrumAttribute<Tango::DevFloat>::generate_data()
{
    static int deg = 0;
    static double PI = 3.1415926;

    for (auto x = 0; x < _dim_x; x++)
    {
        auto r = deg * PI / 180.0;
        _spectrum[x] = cos(r);

        if (_spectrum[x] > 1)
            _spectrum[x] = 1;

        if (_spectrum[x] < -1)
            _spectrum[x] = -1;

        if (++deg == 360)
            deg = 0;
    }
}

//=============================================================================
//=============================================================================
template<>
void SpectrumAttribute<Tango::DevDouble>::generate_data()
{
    static int deg = 0;
    static double PI = 3.1415926;

    for (auto x = 0; x < _dim_x; x++)
    {
        auto r = deg * PI / 180.0;
        _spectrum[x] = sin(r);

        if (_spectrum[x] > 1)
            _spectrum[x] = 1;

        if (_spectrum[x] < -1)
            _spectrum[x] = -1;

        if (++deg == 360)
            deg = 0;
    }
}

//=============================================================================
//=============================================================================
template<>
void SpectrumAttribute<Tango::DevString>::generate_data()
{
    static vector<string> strings = {"this", "is", "a", "classic,", "a", "dish", "that", "will", "never", "go", "out",
        "of", "fashion", "If", "you're", "looking", "for", "a", "treat", "or", "to", "impress", "someone,", "this",
        "is", "at", "the", "top", "of", "my", "list.", "Whenever", "I", "make", "this", "recipe,", "I", "look",
        "forward", "to", "the", "moment", "when", "the", "steaks", "are", "returned", "to", "the", "pan", "to", "be",
        "covered", "in", "sauce.", "At", "that", "point,", "I", "just", "know", "how", "good", "it's", "going", "to",
        "taste"};

    for (auto x = 0; x < _dim_x; x++)
    {
        if(_spectrum[x] != nullptr)
            CORBA::string_free(_spectrum[x]);

        _spectrum[x] = CORBA::string_dup(strings[experimental::randint(0, ((int)strings.size()) - 1)].c_str());
    }
}

//=============================================================================
//=============================================================================
template<>
void SpectrumAttribute<Tango::DevState>::generate_data()
{
    random_device rd;
    mt19937 gen(rd());
    bernoulli_distribution d(0.25);

    for (auto x = 0; x < _dim_x; x++)
        _spectrum[x] = d(gen) ? Tango::ON : Tango::OFF;
}

} // namespace HdbppDataProducer_ns
